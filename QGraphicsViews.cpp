﻿#include "QGraphicsViews.h"
#include <QScrollBar>
#include <QDebug>
#include "ControlItem.h"
#include "BaseItem.h"

QGraphicsViews::QGraphicsViews(QWidget *parent) : QGraphicsView(parent)
{
    this->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);//解决拖动是背景图片残影
    setDragMode(QGraphicsView::ScrollHandDrag);
    drawBg();
    // 隐藏水平/竖直滚动条
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setBackgroundBrush(Qt::gray);

    // 设置场景范围
    setSceneRect(INT_MIN / 2, INT_MIN / 2, INT_MAX, INT_MAX);

    // 反锯齿
    setRenderHints(QPainter::Antialiasing);

    item=new QGraphicsPixmapItem;

    scene=new QGraphicsScenes;
    scene->addItem(item);
    this->setScene(scene);
}

void QGraphicsViews::DispImage(QImage &Image)
{
    image=QPixmap::fromImage(Image);
    item->setPixmap(image);

    GetFit();
}

void QGraphicsViews::SetToFit()
{
    ZoomFrame(ZoomFit);
    QScrollBar *pHbar =  this->horizontalScrollBar();
    pHbar->setSliderPosition(PixX);
    QScrollBar *pVbar = this->verticalScrollBar();
    pVbar->setSliderPosition(PixY);
}

void QGraphicsViews::ClearObj()
{
    foreach(auto item,scene->items())
    {
        if(item->type()==10)
        {
           delete item;
        }
    }
}
void QGraphicsViews::ZoomFrame(double value)
{
    double tmp=value/ZoomValue;
    ZoomValue*=tmp;
    ControlItem::SetScale(ZoomValue);
    BaseItem::SetScale(ZoomValue);
    this->scale(tmp,tmp);
}
void QGraphicsViews::GetFit()
{    
    if(this->width()<1||image.width()<1)
    {
        return;
    }
    //图片自适应方法
    double winWidth=this->width();
    double winHeight=this->height();
    double ScaleWidth=(image.width()+1)/winWidth;
    double ScaleHeight=(image.height()+1)/winHeight;
    double row1,column1;
    double s=0;
    if(ScaleWidth>=ScaleHeight)
    {

        row1= -(1) * ((winHeight * ScaleWidth) - image.height()) / 2;
        column1 = 0  ;
        s=1/ScaleWidth;
    }
    else
    {
        row1= 0;
        column1 = -(1.0) * ((winWidth * ScaleHeight) - image.width()) / 2 ;
        s=1/ScaleHeight;
    }

    if(ZoomFit!=s||PixX!=column1*s)
    {
        ZoomFit=s;
        PixX=column1*s;
        PixY=row1*s;
        SetToFit();
    }
}

void QGraphicsViews::drawBg()
{
    bgPix.fill(color1);
    QPainter painter(&bgPix);
    painter.fillRect(0, 0, 32, 32, color2);
    painter.fillRect(32, 32, 32, 32, color2);
    painter.end();
    //this->update();
}

void QGraphicsViews::mousePressEvent(QMouseEvent *event)
{
    QGraphicsView::mousePressEvent(event);
}
void QGraphicsViews::resizeEvent(QResizeEvent *event)
{
    GetFit();
    QGraphicsView::resizeEvent(event);
}
void QGraphicsViews::mouseReleaseEvent(QMouseEvent *event)
{
    QGraphicsView::mouseReleaseEvent(event);
}
void QGraphicsViews::mouseDoubleClickEvent(QMouseEvent *event)
{
    SetToFit();
    QGraphicsView::mouseDoubleClickEvent(event);
}
void QGraphicsViews::wheelEvent(QWheelEvent *event)
{

    if((event->delta() > 0)&&(ZoomValue >= 50))//最大放大到原始图像的50倍
    {
        return;
    }
    else if((event->delta() < 0)&&(ZoomValue <= 0.01))//图像缩小到自适应大小之后就不继续缩小
    {
        return;
    }
    else
    {
        double tmp=ZoomValue;
        if(event->delta() > 0)//鼠标滚轮向前滚动
        {
            tmp*=1.1;//每次放大10%
        }
        else
        {
            tmp*=0.9;//每次缩小10%
        }
        ZoomFrame(tmp);
        if(event->delta() > 0)
        {
           // moveBy(-event->pos().x()*qrealOriginScale*0.1, -event->pos().y()*qrealOriginScale*0.1);//使图片缩放的效果看起来像是以鼠标所在点为中心进行缩放的
        }
        else
        {
            //moveBy(event->pos().x()*qrealOriginScale*0.1, event->pos().y()*qrealOriginScale*0.1);//使图片缩放的效果看起来像是以鼠标所在点为中心进行缩放的
        }
    }

}

void QGraphicsViews::drawBackground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(rect);
    painter->drawTiledPixmap(rect, bgPix);//绘制背景曾
}



