# QtDrawRegion

#### 介绍
使用qt开发、包含图片显示功能、自适应缩放、背景图片、画roi工具。不依赖其他库纯Qt

#### 软件架构




#### 截图
![image](https://gitee.com/adss22e/qt-draw-region/raw/master/imgs/1.PNG)
![image](https://gitee.com/adss22e/qt-draw-region/raw/master/imgs/2.PNG)
![image](https://gitee.com/adss22e/qt-draw-region/raw/master/imgs/3.PNG)

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

https://blog.csdn.net/qq_34139994/article/details/105878101



#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
