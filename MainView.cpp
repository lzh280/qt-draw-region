﻿#include "MainView.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include "BaseItem.h"
QList<QPushButton* > btnlist;
MainView::MainView(QWidget *parent) : QWidget(parent)
{
    view=new QGraphicsViews;

    QList<QString> namelist;
    namelist<<QString::fromLocal8Bit("矩形")<<QString::fromLocal8Bit("矩形旋转");
    namelist<<QString::fromLocal8Bit("圆形")<<QString::fromLocal8Bit("椭圆旋转");
    namelist<<QString::fromLocal8Bit("同心圆")<<QString::fromLocal8Bit("饼图");
    namelist<<QString::fromLocal8Bit("多边形")<<QString::fromLocal8Bit("圆端矩形");
    namelist<<QString::fromLocal8Bit("圆角矩形")<<QString::fromLocal8Bit("直线-无");
    namelist<<QString::fromLocal8Bit("卡尺-无")<<QString::fromLocal8Bit("锚点-无");
    namelist<<QString::fromLocal8Bit("打开图片")<<QString::fromLocal8Bit("清空");
    QList<QString> objname;
    objname<<"a1"<<"a2";
    objname<<"a3"<<"a4";
    objname<<"a5"<<"a6";
    objname<<"a7"<<"a8";
    objname<<"a9"<<"a10";
    objname<<"a11"<<"a12";
    objname<<"a13"<<"a14";

    auto BtnArea=new QVBoxLayout;
    QSize sz(80,30);
    for (int i=0;i<objname.length();i++)
    {
        auto btn=new QPushButton;
        btn->setFixedSize(sz);
        btn->setText(namelist[i]);
        btn->setObjectName(objname[i]);
        BtnArea->addWidget(btn);
        btn->installEventFilter(this);
        btnlist<<btn;
    }

    auto MainArea=new QHBoxLayout;
    MainArea->addLayout(BtnArea);
    MainArea->addWidget(view);

    this->setLayout(MainArea);
}

bool MainView::eventFilter(QObject *obj, QEvent *event)
{
    QMouseEvent *MouseEvent = static_cast<QMouseEvent *>(event);

    if((MouseEvent->buttons() == Qt::LeftButton)&&(event->type() == QEvent::MouseButtonPress))
    {
        QString Name=obj->objectName();
        QAbstractButton *CkBtn = qobject_cast<QAbstractButton *>(obj);
        if(Name=="a1")
        {
            auto obj=new RectangleItem(200,200,200,200);
            view->scene->addItem(obj);
        }
        if(Name=="a2")
        {
            auto obj=new RectangleRItem(200,200,200,200,0);
            view->scene->addItem(obj);
        }
        if(Name=="a3")
        {
            auto obj=new CircleItem(200,200,300);
            view->scene->addItem(obj);
        }
        if(Name=="a4")
        {
            auto obj=new EllipseItem(200,200,300,200,0.2);
            view->scene->addItem(obj);
        }
        if(Name=="a5")
        {
            auto obj=new ConcentricCircleItem(200,200,200,300);
            view->scene->addItem(obj);
        }
        if(Name=="a6")
        {
            auto obj=new PieItem(200,200,200,3.14/3,0);
            view->scene->addItem(obj);
        }
        if(Name=="a7")
        {
            view->scene->startCreate();
            for (int i=0;i<btnlist.length();i++)
            {
                btnlist[i]->setEnabled(false);
            }
            //setBtnEnabled(false);
            PolygonItem *m_polygon = new PolygonItem();
            view->scene->addItem(m_polygon);

            connect(view->scene, SIGNAL(updatePoint(QPointF, QList<QPointF>, bool)), m_polygon, SLOT(pushPoint(QPointF, QList<QPointF>, bool)));
            connect(view->scene, &QGraphicsScenes::createFinished, [=]()
            {
                for (int i=0;i<btnlist.length();i++)
                {
                    btnlist[i]->setEnabled(true);
                }
            });
        }
        if(Name=="a8")
        {
            auto obj=new RoundRectangle1Item(0,0,200,300);
            view->scene->addItem(obj);
        }
        if(Name=="a9")
        {
            auto obj=new RoundRectangle2Item(0,0,300,200,0);
            view->scene->addItem(obj);
        }
        if(Name=="a10")
        {

        }
        if(Name=="a11")
        {

        }
        if(Name=="a12")
        {

        }
        if(Name=="a13")
        {
            QImage img("a.bmp");
            view->DispImage(img);
        }
        if(Name=="a14")
        {
            view->ClearObj();
        }
    }


return QObject::eventFilter(obj, event);

}
