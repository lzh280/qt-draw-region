﻿#ifndef MAINVIEW_H
#define MAINVIEW_H

#include <QWidget>
#include "QGraphicsViews.h"


class MainView : public QWidget
{
    Q_OBJECT
public:
    explicit MainView(QWidget *parent = nullptr);

    QGraphicsViews* view;
protected:
    bool eventFilter(QObject *obj, QEvent *event);
signals:

};

#endif // MAINVIEW_H
